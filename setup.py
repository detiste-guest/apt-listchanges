import re
from setuptools import setup, find_packages
from textwrap import dedent

def get_version_from_changelog() -> str:
    with open('debian/changelog', 'rt', encoding='utf-8') as f:
        first_line = next(f)
        match = re.search(r'\(([^+\(\)]+).*?\)', first_line)
        if not match:
            raise ValueError(
                'Could not find version number on first changelog line')
        return match[1]


setup(
    name='apt-listchanges',
    version=get_version_from_changelog(),
    author='Jonathan Kamens',
    author_email='jik@kamens.us',
    description='Show new changelog entries from Debian package archives',
    url='https://salsa.debian.org/debian/apt-listchanges',
    long_description=dedent('''\
        The tool apt-listchanges can compare a new version of a
        package with the one currently installed and show what has been
        changed, by extracting the relevant entries from the Debian changelog
        and NEWS files.

        It can be run on several .deb archives at a time to get a list of all
        changes that would be caused by installing or upgrading a group of
        packages. When configured as an APT plugin it will do this
        automatically during upgrades.'''),
    long_description_content_type='text/markdown',
    license='GPL2+',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',  # noqa
        'Natural Language :: English',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: System :: Monitoring',
        'Topic :: System :: Systems Administration',
    ],
    packages=find_packages(),
    package_data={
        'apt_listchanges': [
            'apt-listchanges.ui',
        ],
    },
    python_requires='>=3.7',
    # Can't list debconf or apt_pkg here, they're not on PyPI
    setup_requires=['pytest', 'pyyaml'],
    install_requires=['debconf'],
    entry_points={
        'console_scripts': [
            "apt-listchanges = apt_listchanges.apt_listchanges:main",
        ],
    },
)
