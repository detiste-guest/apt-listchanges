import os

from utils import (
    install_yaml_package,
    make_config,
    package_from_yaml_file,
    setup_filterer,
)
from apt_listchanges.ALCLog import set_debug
from apt_listchanges.ALCSeenDb import SeenDb
from apt_listchanges.apt_listchanges import process_pkgs
from apt_listchanges.frontends import Frontend


# Make sure we don't get confused by a package name in the changelog that's
# different from the binary package name we're considering.
def test_bug1053725(fs) -> None:
    testdir = os.path.dirname(__file__)
    fs.add_real_directory(testdir)
    install_yaml_package(fs, 'tor_0.4.8.6-1_amd64.yaml')
    config = make_config({'no_network': False, 'which': 'news'})
    set_debug(True)
    frontend = Frontend(config, 1)
    seen_db = SeenDb()
    filterer = setup_filterer(seen_db=seen_db)
    new_yamls = ['tor-geoipdb_0.4.8.7-1_all.yaml',
                 'tor_0.4.8.7-1_amd64.yaml']
    pkgs = [package_from_yaml_file(f, filterer=filterer) for f in new_yamls]
    (news, _changelog) = process_pkgs(config, frontend, seen_db, pkgs)
    assert not news
