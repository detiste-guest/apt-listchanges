pkgdata:
  name: tor
  version: 0.4.8.6-1
  source: tor
  source_version: 0.4.8.6-1
  arch: amd64
files:
  /usr/share/doc/tor/changelog.gz: |
    Changes in version 0.4.8.6 - 2023-09-18
      This version contains an important fix for onion service regarding congestion
      control and its reliability. Apart from that, uneeded BUG warnings have been
      suppressed especially about a compression bomb seen on relays. We strongly
      recommend, in particular onion service operators, to upgrade as soon as
      possible to this latest stable.

      o Major bugfixes (onion service):
        - Fix a reliability issue where services were expiring their
          introduction points every consensus update. This caused
          connectivity issues for clients caching the old descriptor and
          intro points. Bug reported and fixed by gitlab user
          @hyunsoo.kim676. Fixes bug 40858; bugfix on 0.4.7.5-alpha.

      o Minor features (debugging, compression):
        - Log the input and output buffer sizes when we detect a potential
          compression bomb. Diagnostic for ticket 40739.

      o Minor features (fallbackdir):
        - Regenerate fallback directories generated on September 18, 2023.

      o Minor features (geoip data):
        - Update the geoip files to match the IPFire Location Database, as
          retrieved on 2023/09/18.

      o Minor bugfix (defensive programming):
        - Disable multiple BUG warnings of a missing relay identity key when
          starting an instance of Tor compiled without relay support. Fixes
          bug 40848; bugfix on 0.4.3.1-alpha.

      o Minor bugfixes (bridge authority):
        - When reporting a pseudo-networkstatus as a bridge authority, or
          answering "ns/purpose/*" controller requests, include accurate
          published-on dates from our list of router descriptors. Fixes bug
          40855; bugfix on 0.4.8.1-alpha.

      o Minor bugfixes (compression, zstd):
        - Use less frightening language and lower the log-level of our run-
          time ABI compatibility check message in our Zstd compression
          subsystem. Fixes bug 40815; bugfix on 0.4.3.1-alpha.


    Changes in version 0.4.8.5 - 2023-08-30
      Quick second release after the first stable few days ago fixing minor
      annoying bugfixes creating log BUG stacktrace. We also fix BSD compilation
      failures and PoW unit test.

      o Minor features (fallbackdir):
        - Regenerate fallback directories generated on August 30, 2023.

      o Minor features (geoip data):
        - Update the geoip files to match the IPFire Location Database, as
          retrieved on 2023/08/30.

      o Minor bugfix (NetBSD, compilation):
        - Fix compilation issue on NetBSD by avoiding an unnecessary
          dependency on "huge" page mappings in Equi-X. Fixes bug 40843;
          bugfix on 0.4.8.1-alpha.

      o Minor bugfix (NetBSD, testing):
        - Fix test failures in "crypto/hashx" and "slow/crypto/equix" on
          x86_64 and aarch64 NetBSD hosts, by adding support for
          PROT_MPROTECT() flags. Fixes bug 40844; bugfix on 0.4.8.1-alpha.

      o Minor bugfixes (conflux):
        - Demote a relay-side warn about too many legs to ProtocolWarn, as
          there are conditions that it can briefly happen during set
          construction. Also add additional set logging details for all
          error cases. Fixes bug 40841; bugfix on 0.4.8.1-alpha.
        - Prevent non-fatal assert stacktrace caused by using conflux sets
          during their teardown process. Fixes bug 40842; bugfix
          on 0.4.8.1-alpha.
  /usr/share/doc/tor/NEWS.Debian.gz: |
    tor (0.2.0.26-rc-1) experimental; urgency=critical

      * weak cryptographic keys

        It has been discovered that the random number generator in Debian's
        openssl package is predictable.  This is caused by an incorrect
        Debian-specific change to the openssl package (CVE-2008-0166).  As a
        result, cryptographic key material may be guessable.

        See Debian Security Advisory number 1571 (DSA-1571) for more information:
        http://lists.debian.org/debian-security-announce/2008/msg00152.html

        If you run a Tor server using this package please see
        /var/lib/tor/keys/moved-away-by-tor-package/README.REALLY

     -- Peter Palfrader <weasel@debian.org>  Tue, 13 May 2008 12:49:05 +0200
  /usr/share/doc/tor/changelog.Debian.gz: |
    tor (0.4.8.6-1) unstable; urgency=medium

      * New upstream version.

     -- Peter Palfrader <weasel@debian.org>  Mon, 18 Sep 2023 20:07:19 +0200

    tor (0.4.8.5-1) unstable; urgency=medium

      * New upstream version.
      * Retire postinst code that deals with broken keys from 2008
        and apparmor migrations from 2012.
      * tor.postinst: use command -v in place of which.

     -- Peter Palfrader <weasel@debian.org>  Wed, 30 Aug 2023 20:19:26 +0200
apt_changelog: |
  tor (0.4.8.6-1) unstable; urgency=medium

    * New upstream version.

   -- Peter Palfrader <weasel@debian.org>  Mon, 18 Sep 2023 20:07:19 +0200

  tor (0.4.8.5-1) unstable; urgency=medium

    * New upstream version.
    * Retire postinst code that deals with broken keys from 2008
      and apparmor migrations from 2012.
    * tor.postinst: use command -v in place of which.

   -- Peter Palfrader <weasel@debian.org>  Wed, 30 Aug 2023 20:19:26 +0200
