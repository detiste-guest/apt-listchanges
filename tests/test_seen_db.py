# pylint: disable=protected-access

import os
import re
import tempfile
import time

import pytest

from apt_listchanges.ALCSeenDb import DbError, SeenDb
from apt_listchanges.DebianFiles import ChangelogEntry


def test_corrupt_db() -> None:
    with pytest.raises(DbError):
        SeenDb('/dev/null')


def test_read_write_db() -> None:
    with tempfile.NamedTemporaryFile() as file_obj:
        os.unlink(file_obj.name)
        seen_db = SeenDb(file_obj.name)
        entry = ChangelogEntry('foo (1.0-1) unstable; urgency=low',
                               '/usr/share/doc/foo/changelog.Debian.gz',
                               'foo', 'foo', '1.0-1', 'low')
        entry.add_content('  Here is some content.\n')
        entry.set_trailer(' -- Nice Guy <example@example.com> '
                          'Thu, 01 Jan 1970 00:00:00 -0000\n')
        seen_db.add(entry)
        seen_db.apply_changes()
        seen_db = SeenDb(file_obj.name)
        assert entry in seen_db
        os.unlink(f'{file_obj.name}-old')


def test_no_source() -> None:
    seen_db = SeenDb()
    entry = ChangelogEntry('foo (1.0-1) unstable; urgency=low',
                           '/usr/share/doc/foo/changelog.Debian.gz',
                           'foo', 'foo', '1.0-1', 'low')
    entry.add_content('  Here is some content.\n')
    entry.set_trailer(' -- Nice Guy <example@example.com> '
                      'Thu, 01 Jan 1970 00:00:00 -0000\n')
    assert not seen_db.is_in_source(entry)


def test_getitem() -> None:
    seen_db = SeenDb()
    entry = ChangelogEntry('foo (1.0-1) unstable; urgency=low',
                           '/usr/share/doc/foo/changelog.Debian.gz',
                           'foo', 'foo', '1.0-1', 'low')
    entry.add_content('  Here is some content.\n')
    entry.set_trailer(' -- Nice Guy <example@example.com> '
                      'Thu, 01 Jan 1970 00:00:00 -0000\n')
    before = int(time.time())
    seen_db.add(entry)
    then = seen_db[entry]
    after = int(time.time())
    assert before <= then <= after


def test_getitem_keyerror() -> None:
    seen_db = SeenDb()
    entry = ChangelogEntry('foo (1.0-1) unstable; urgency=low',
                           '/usr/share/doc/foo/changelog.Debian.gz',
                           'foo', 'foo', '1.0-1', 'low')
    entry.add_content('  Here is some content.\n')
    entry.set_trailer(' -- Nice Guy <example@example.com> '
                      'Thu, 01 Jan 1970 00:00:00 -0000\n')
    with pytest.raises(KeyError):
        print(seen_db[entry])


def test_setitem() -> None:
    seen_db = SeenDb()
    entry = ChangelogEntry('foo (1.0-1) unstable; urgency=low',
                           '/usr/share/doc/foo/changelog.Debian.gz',
                           'foo', 'foo', '1.0-1', 'low')
    entry.add_content('  Here is some content.\n')
    entry.set_trailer(' -- Nice Guy <example@example.com> '
                      'Thu, 01 Jan 1970 00:00:00 -0000\n')
    before = int(time.time())
    seen_db[entry] = True
    then = seen_db[entry]
    after = int(time.time())
    assert before <= then <= after


def test_dump(capsys) -> None:
    seen_db = SeenDb()
    entry = ChangelogEntry('foo (1.0-1) unstable; urgency=low',
                           '/usr/share/doc/foo/changelog.Debian.gz',
                           'foo', 'foo', '1.0-1', 'low')
    entry.add_content('  Here is some content.\n')
    entry.set_trailer(' -- Nice Guy <example@example.com> '
                      'Thu, 01 Jan 1970 00:00:00 -0000\n')
    seen_db.add(entry)
    seen_db.dump()
    captured = capsys.readouterr()
    assert re.match(r'sources:\n  foo:\n    \S+  \d+ \(\d+-\d+-\d+\)\n'
                    r'packages:\n  foo:\n    /usr/share/doc/foo/changelog\.'
                    r'Debian\.gz:\n      \S+  \d+ \(\d+-\d+-\d+\)\n$',
                    captured.out)


def replace_timestamp(d, timestamp) -> None:
    for key, value in d.items():
        if isinstance(value, dict):
            replace_timestamp(value, timestamp)
        else:
            d[key] = timestamp


def test_expire_old() -> None:
    seen_db = SeenDb()
    entry = ChangelogEntry('foo (1.0-1) unstable; urgency=low',
                           '/usr/share/doc/foo/changelog.Debian.gz',
                           'foo', 'foo', '1.0-1', 'low')
    entry.add_content('  Here is some content.\n')
    entry.set_trailer(' -- Nice Guy <example@example.com> '
                      'Thu, 01 Jan 1970 00:00:00 -0000\n')
    seen_db.add(entry)
    seen_db.apply_changes()
    # Gotta muck around inside the seen DB for this test, alas.
    replace_timestamp(seen_db._d, time.time() - 365 * 4)
    assert entry in seen_db
    seen_db.expire_old()
    assert entry not in seen_db


def test_initial_save() -> None:
    with tempfile.NamedTemporaryFile() as f:
        filename = f.name
    # We're outside the context manager so the file should be gone
    seen_db = SeenDb(filename)
    seen_db.apply_changes()
