#!/usr/bin/env python3

import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))

from utils import deb_to_yaml  # noqa pylint: disable=wrong-import-position


def main() -> None:
    deb_to_yaml(sys.argv[1])


if __name__ == '__main__':
    main()
