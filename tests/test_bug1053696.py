import os

from utils import (
    install_yaml_package,
    make_config,
    package_from_yaml_file,
    setup_filterer,
)
from apt_listchanges.ALCSeenDb import SeenDb
from apt_listchanges.apt_listchanges import process_pkgs
from apt_listchanges.frontends import Frontend


# Make sure version filtering on source package version works when fetching
# changelogs via apt.
def test_bug1053696(fs) -> None:
    testdir = os.path.dirname(__file__)
    fs.add_real_directory(testdir)
    install_yaml_package(fs, 'libpython3.11-dev_3.11.6-2_amd64.yaml')
    config = make_config({'no_network': False})
    frontend = Frontend(config, 1)
    seen_db = SeenDb()
    filterer = setup_filterer(seen_db=seen_db)
    pkgs = [package_from_yaml_file('libpython3.11-dev_3.11.6-3_amd64.yaml',
                                   filterer=filterer)]
    (news, changelog) = process_pkgs(config, frontend, seen_db, pkgs)
    assert not news
    print("CHANGELOG:\n", changelog)
    assert changelog.startswith('python3.11 (3.11.6-3)')
    assert changelog.endswith('08 Oct 2023 07:06:43 +0200\n')
