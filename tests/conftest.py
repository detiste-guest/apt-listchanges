import apt_pkg


def pytest_configure() -> None:
    apt_pkg.init()
