# vim:set fileencoding=utf-8 et ts=4 sts=4 sw=4:
#
#   apt-listchanges - Show changelog entries between the installed versions
#                     of a set of packages and the versions contained in
#                     corresponding .deb files
#
#   Copyright (C) 2016       Robert Luberda  <robert@debian.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
#

import sys

from apt_listchanges.ALChacks import _

debug_enabled = False


def set_debug(should_debug: bool) -> None:
    global debug_enabled  # pylint: disable=global-statement
    debug_enabled = should_debug


def error(msg: str) -> None:
    print(_("apt-listchanges: %(msg)s") % {'msg': msg}, file=sys.stderr)


def warning(msg: str) -> None:
    print(_("apt-listchanges warning: %(msg)s") % {'msg': msg},
          file=sys.stderr)


def info(msg: str) -> None:
    print(_("apt-listchanges: %(msg)s") % {'msg': msg}, file=sys.stdout)


def debug(msg: str) -> None:
    if debug_enabled:
        print(msg, file=sys.stdout)
