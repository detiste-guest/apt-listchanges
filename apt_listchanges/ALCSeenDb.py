# vim:set fileencoding=utf-8 et ts=4 sts=4 sw=4:
#
#   apt-listchanges - Show changelog entries between the installed versions
#                     of a set of packages and the versions contained in
#                     corresponding .deb files
#
#   Copyright (C) 2000-2006  Matt Zimmerman  <mdz@debian.org>
#   Copyright (C) 2006       Pierre Habouzit <madcoder@debian.org>
#   Copyright (C) 2016       Robert Luberda  <robert@debian.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
#
from __future__ import annotations

import copy
import datetime
import hashlib
import os
import pickle
import time
from typing import Any, TYPE_CHECKING

from apt_listchanges.ALChacks import _

if TYPE_CHECKING:
    from apt_listchanges.DebianFiles import ChangelogEntry

class DbError(Exception):
    pass


class SeenDb():
    '''Tracker for seen per-package and per-source changelog entries
    Maintained in memory during program execution, potentially persisted to
    disk with pickle between invocations.
    Per binary package name per changelog path within that package, store
    checksums of changelog entries along with timestamps of when they were
    added to the database.
    Per source package name, store checksums of entry contents (sans headers
    and footers) along with timestamps of when they were added to the database.
    '''

    # XXX: please make this a tiny object
    blank_db = {
        'db_version': 1,
        'sources': {},
        'packages': {},
    }

    def __init__(self, path: str | None = None) -> None:
        '''Initialize seen DB, possibly from disk
        If path is None or does not exist on disk then an empty DB is
        initialized. Otherwise content is loaded from disk.'''
        self.path = path
        self._changes = {}
        if not path:
            self._d = copy.deepcopy(self.blank_db)
            self.merged = {}
            return
        self._d = self.read_database()
        self.merged = {chk: stamp
                       for package in self._d['packages'].values()
                       for file in package.values()
                       for chk, stamp in file.items()}

    def read_database(self):
        try:
            # pylint: disable=consider-using-with
            f = open(self.path, 'rb')
        except FileNotFoundError:
            return copy.deepcopy(self.blank_db)
        try:
            return pickle.load(f)
        except Exception as ex:
            raise DbError(_("Database %(db)s failed to load: %(errmsg)s")
                          % {'db': self.path, 'errmsg': str(ex)}) from ex
        finally:
            f.close()

    def __contains__(self, entry: ChangelogEntry) -> bool:
        '''Return true if entry is present for source or binary package'''
        # We check both source and binary package because it's possible, albeit
        # rare, for a binary package to switch to a different source package.
        return self.is_in_source(entry) or \
            self.merged.get(self._full_chk(entry), False)

    def is_in_source(self, entry: ChangelogEntry):
        '''Check if in source, return timestamp if so'''
        if entry.source not in self._d['sources']:
            return False
        digest = self._partial_chk(entry)
        return self._d['sources'][entry.source].get(digest, False)

    def is_in_binary(self, entry: ChangelogEntry):
        '''Check if in package, return timestamp if so'''
        if entry.package not in self._d['packages']:
            return False
        digest = self._full_chk(entry)
        return any(self._d['packages'][entry.package][path].get(digest, False)
                   for path in self._d['packages'][entry.package].keys())

    def __getitem__(self, entry: ChangelogEntry) -> int:
        '''Return timestamp value for binary or source package for entry'''
        package_stamp = self.is_in_binary(entry)
        source_stamp = self.is_in_source(entry)
        if not (package_stamp or source_stamp):
            raise KeyError(entry)
        return max(package_stamp or 0, source_stamp or 0)

    def __setitem__(self, entry: ChangelogEntry, value: Any) -> None:
        '''Add item to DB. Value is ignored.'''
        self.add(entry)

    def has_package(self, package: str) -> bool:
        return package in self._d['packages']

    def add_package(self, package: str) -> None:
        change = {'packages': {package: {}}}
        self._merge_in(self._d, change)
        self._merge_in(self._changes, change)

    def add(self, entry: ChangelogEntry) -> None:
        '''Add the specified entry to the seen DB if not already there'''
        now = int(time.time())
        entry_chk = self._full_chk(entry)
        change = {
            'packages': {
                entry.package: {
                    entry.path: {
                        entry_chk: now
                    }
                }
            },
            'sources': {
                entry.source: {
                    self._partial_chk(entry): now
                }
            }
        }
        self.merged[entry_chk] = now
        self._merge_in(self._d, change)
        self._merge_in(self._changes, change)

    def _merge_in(
        self,
        target,
        src
    ) -> None:
        '''Merge new values from src into target, not overwriting'''
        for key, value in src.items():
            if isinstance(value, dict):
                self._merge_in(target.setdefault(key, {}), value)
            elif key not in target:
                target[key] = value

    @staticmethod
    def _full_chk(entry: ChangelogEntry) -> bytes:
        return hashlib.md5(str(entry).strip().encode()).digest()

    @staticmethod
    def _partial_chk(entry: ChangelogEntry) -> bytes:
        return hashlib.md5((entry.content + entry.trailer).strip()
                           .encode()).digest()

    @staticmethod
    def _checksum_line(checksum, stamp) -> str:
        return (f'{"".join((f"{c:0{2}x}" for c in checksum))} '
                f' {stamp} '
                f'({datetime.datetime.fromtimestamp(stamp).strftime("%F")})')

    def expire_old(self) -> None:
        # We keep entries for three years because Debian releases come out
        # approximately annually and are supported for approximately two years,
        # so the odds are extremely good that a new package version is released
        # in the interim. But it's OK if that doesn't happen for all packages
        # since we fall back on re-parsing the changelog on disk when
        # necessary, which will "re-up" things for another three years.
        three_years_ago = int(time.time() - 365.25 * 3)
        for sources, checksums in self._d['sources'].items():
            # Convert to list so we can safely modify the dict in the loop
            for checksum in list(checksums.keys()):
                if checksums[checksum] < three_years_ago:
                    del checksums[checksum]
        for packages, paths in self._d['packages'].items():
            for path, checksums in paths.items():
                for checksum in list(checksums.keys()):
                    if checksums[checksum] < three_years_ago:
                        del checksums[checksum]
        self.merged = {chk: stamp
                       for package in self._d['packages'].values()
                       for file in package.values()
                       for chk, stamp in file.items()}

    def apply_changes(self) -> None:
        if not self.path:
            self._merge_in(self._d, self._changes)
            self._changes = {}
            return
        oldname = f'{self.path}-old'
        newname = f'{self.path}-new'
        # We have to create the file in order to lock it!
        reread_db = os.path.exists(self.path)
        with open(self.path, 'ab') as lockable:
            lockable.seek(0)
            os.lockf(lockable.fileno(), os.F_LOCK, 0)
            # In case somebody else modified it while we were working
            if reread_db:
                self._d = self.read_database()
            self._merge_in(self._d, self._changes)
            self._changes = {}
            self.expire_old()
            with open(newname, 'wb') as f:
                pickle.dump(self._d, f)
            try:
                os.unlink(oldname)
            except FileNotFoundError:
                pass
            os.link(self.path, oldname)
            os.rename(newname, self.path)

    def dump(self) -> None:
        print('sources:')
        for source, checksums in self._d['sources'].items():
            print(f'  {source}:')
            for checksum, stamp in checksums.items():
                print(f'    {self._checksum_line(checksum, stamp)}')
        print('packages:')
        for package, files in self._d['packages'].items():
            print(f'  {package}:')
            for path, checksums in files.items():
                print(f'    {path}:')
                for checksum, stamp in checksums.items():
                    print(f'      {self._checksum_line(checksum, stamp)}')
